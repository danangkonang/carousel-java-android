package com.carousel.carousel;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.carousel.R;

public class FragmentCarousel extends Fragment {
    private static final String ARG_PARAM1 = "params";

    private String imageUrls;

    public FragmentCarousel() {
    }

    public static FragmentCarousel newInstance(String params) {
        FragmentCarousel fragment = new FragmentCarousel();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, params);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        imageUrls = getArguments().getString(ARG_PARAM1);
        View view = inflater.inflate(R.layout.fragment_slider_item, container, false);
        ImageView img = (ImageView) view.findViewById(R.id.img);
        Glide.with(getActivity())
                .load(imageUrls)
                .placeholder(R.drawable.image_slider_1)
                .into(img);
        return view;
    }
}
