package com.carousel;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.widget.LinearLayout;

import com.carousel.carousel.CarouselAdapter;
import com.carousel.carousel.CarouselIndicator;
import com.carousel.carousel.CarouselView;
import com.carousel.carousel.FragmentCarousel;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {


    private CarouselAdapter mAdapter;
    private CarouselIndicator mIndicator;

    private CarouselView sliderView;
    private LinearLayout mLinearLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sliderView = (CarouselView) findViewById(R.id.sliderView);
        mLinearLayout = (LinearLayout) findViewById(R.id.pagesContainer);
        setupSlider();
    }

    private void setupSlider() {
        sliderView.setDurationScroll(800);
        List<Fragment> fragments = new ArrayList<>();
        fragments.add(FragmentCarousel.newInstance("http://www.menucool.com/slider/prod/image-slider-1.jpg"));
        fragments.add(FragmentCarousel.newInstance("http://www.menucool.com/slider/prod/image-slider-2.jpg"));
        fragments.add(FragmentCarousel.newInstance("http://www.menucool.com/slider/prod/image-slider-3.jpg"));
        fragments.add(FragmentCarousel.newInstance("http://www.menucool.com/slider/prod/image-slider-4.jpg"));

        mAdapter = new CarouselAdapter(getSupportFragmentManager(), fragments);
        sliderView.setAdapter(mAdapter);
        mIndicator = new CarouselIndicator(this, mLinearLayout, sliderView, R.drawable.indicator_circle);
        mIndicator.setPageCount(fragments.size());
        mIndicator.show();
    }
}